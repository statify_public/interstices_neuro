# interstices_neuro

Annexes de l'article Inria interstices

NB. Pour générer le notebook jupyter, lancer https://mybinder.org/ puis taper l'URL https://gitlab.inria.fr/statify_public/interstices_neuro.git en choisissant Git Repository.

L'URL générée et à communiquer à l'utilisateur final est
https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fstatify_public%2Finterstices_neuro.git/HEAD

**TODO** : placer un fichier Dockerfile à la racine au lieu de environment.yml (voir https://mybinder.readthedocs.io/en/latest/examples/sample_repos.html#dockerfile-environments )