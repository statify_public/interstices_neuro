# -*- coding: utf-8 -*-
"""
Created on Mon Jul 20 13:53:32 2020

@author: jiaji
"""

import numpy as np
from scipy.spatial import Delaunay
import matplotlib.pyplot as plt
import pandas as pd
import scipy as sp
# import esda
import libpysal as lps
from libpysal.weights import W
from libpysal.weights import KNN
import networkx as nx
import scipy.spatial as sptl
import scipy.sparse as sprs

def delaunay(x, y, boolean_weights=True):
    """
    Delaunay with inverse Euclidean distance as weights
    """
    points = np.vstack((x, y)).T
    tri = Delaunay(points)
    n = len(x)
    adjmat = np.zeros((n,n))
    for i in sp.arange(0, sp.shape(tri.simplices)[0]):
        for j in tri.simplices[i]:
            if j < n:
                 adjmat[j, tri.simplices[i][tri.simplices[i] < n]] = 1
    nd = {}
    wd = {}
    for i in range(n):
        neighbor=[]
        weight=[]
        j = 0
        for j in range(n):
            if(j!=i):
                if (adjmat[i,j]==1):
                    neighbor.append(j)
                    d = 1/np.sqrt((x[i]-x[j])**2+(y[i]-y[j])**2)
                    weight.append(d)
        nd[i]=neighbor
        wd[i]=weight
    if boolean_weights:
        w = W(nd)
    else:
        w = W(nd,wd)
    return w, nd

def gabriel(x, y, boolean_weights=True):
    """
    Gabriel graph
    """
    points = np.vstack((x, y)).T
    tri = Delaunay(points)
    
    # Convert tri to a standard adjacency matrix
        # Create an empty list-of-list matrix which allows building by row
    lil = sprs.lil_matrix((tri.npoints, tri.npoints))
        # Scan through Delaunay triangulation to retrieve pairs
    indices, indptr = tri.vertex_neighbor_vertices
    for k in range(tri.npoints):
        lil.rows[k] = indptr[indices[k]:indices[k+1]]
        lil.data[k] = np.ones_like(lil.rows[k])  # dummy data of same shape as row
        # Convert to coo format for direct access to each node-edge-node pair
    coo = lil.tocoo()
        # conns contains the index of 2 nodes at each end of an edge
    conns = np.vstack((coo.row, coo.col)).T
        # Convert to upper triangular
    delaunay_conns = np.sort(conns, axis=1)
    # delaunay_conns is the adjacency matrix for the Delanauy triangulation in the COO sparse format, so that each row contains the (row, col) location on a non-zero element. 

    # Trim Edges from Delaunay tessellation to get Gabriel graph
    # The Gabriel network is a subset of the Delaunay, so we just need to prune non-Gabriel edges. A connection between two points is considered a Gabriel egde if you can connect the points by a circle, and that circle contains no other points.

    # The following code performs this check for each edge in the Delanay tessellation by finding the midpoint of the edge, calculating the radius of the circle centered on that midpoint, then finding if any points are within that radius. If yes, then that edge is NOT a Gabriel edge.

    # Find centroid or midpoint of each edge in conns
    c = tri.points[delaunay_conns]
    m = (c[:, 0, :] + c[:, 1, :])/2
    # Find the radius sphere between each pair of nodes
    r = np.sqrt(np.sum((c[:, 0, :] - c[:, 1, :])**2, axis=1))/2

    # In order to see if any of the base points are closer to a given midpoint than r, we use scipy's KDTree class for efficient searching of nearest neighbors. We create a KDTree of the base points, then query the tree to find the distance of the nearest basepoint for each midpoint in m.

    # Use the kd-tree function in Scipy's spatial module
    tree = sptl.cKDTree(points)
    # Find the nearest point for each midpoint
    n = tree.query(x=m, k=1)[0]
    # If nearest point to m is at a distance r, then the edge is a Gabriel edge
    g = n >= r*(0.999)  # The factor is to avoid precision errors in the distances
    # Reduce the connectivity to all True values found in g
    gabriel_conns = delaunay_conns[g]
    
    # Compute weights
    n = len(x)
    # Adjacency matrix
    adjmat = np.zeros((n,n))
    for i in np.arange(0, gabriel_conns.shape[0]):
        adjmat[gabriel_conns[i,0], gabriel_conns[i,1]] = 1
        adjmat[gabriel_conns[i,1], gabriel_conns[i,0]] = 1
        
    nd = {} # neighbours
    wd = {} # weights

    for i in range(n):
        neighbor=[]
        weight=[]
        j = 0
        for j in range(n):
            if(j!=i):
                if (adjmat[i,j]==1):
                    neighbor.append(j)
                    d = 1/np.sqrt((x[i]-x[j])**2+(y[i]-y[j])**2)
                    weight.append(d)
        nd[i]=neighbor
        wd[i]=weight
        
    if boolean_weights:
        w = W(nd)
    else:
        w = W(nd,wd)
        
    return w, nd

def knn(data,k):
    w = KNN.from_array(data, k)
    return w

def distanceBand(data,threshold,binary):
    w=lps.weights.DistanceBand(data,threshold=threshold,binary=binary)
    return w


def creatGraph(data_name,data_path, graph_name, k, distance, boolean_weights=True):
    """
    :TODO: add neighbours to knn and distanceBand returns for the sake of homogeneity?
    """
    df = pd.read_csv(data_path + data_name +".txt")
    df = df.dropna(subset=["Longest_Length"])
    x = np.array(df["Xcoordinates"])
    y = np.array(df["Ycoordinates"])
    z = df["Longest_Length"]
    data = []
    for i in range(0,len(x)):
        data.append((x[i],y[i]))
    
    if (graph_name=="Delaunay"):
        w,nd = delaunay(x,y,boolean_weights)
    elif (graph_name=="Gabriel"):
        w,nd = gabriel(x,y,boolean_weights)        
    elif (graph_name=="KNN"):
        w = knn(data,k)
    elif (graph_name=="Distance"):
        w = distanceBand(data,distance,False)
    else:
        print("Not a valid graph model. Choose from Gabriel, Delaunay, KNN or Distance")
    
    n = len(x)
    v = w.max_neighbors
    nmat = -np.ones((n,int(v)),dtype=int)
    for i in range(n):
        l=0
        for value in w[i]:
            nmat[i,l] = value
            l=l+1
    
    title=[]
    for i in range(0,v):
        t="v"+str(i)
        title.append(t)
        
    nb_mat = pd.DataFrame(nmat,columns=title)
    nb_mat.to_csv(data_path + data_name + "_" + graph_name+".csv",index=None)
    return w
